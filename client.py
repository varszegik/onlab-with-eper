#!~/miniconda3/bin/python3

#%%
import sys
import time

import pandas as pd
import torch
from sklearn.preprocessing import Normalizer


#%%
class FeatureDataset(torch.utils.data.Dataset):
    def __init__(self, file_name):
        file_in = pd.read_csv(file_name, header=None)
        x = file_in.iloc[0:, 1:].values
        y = file_in.iloc[0:, 0].values

        x_train = Normalizer().transform(x)
        y_train = y
        self.X_train = torch.tensor(x_train, dtype=torch.float32)
        self.y_train = torch.tensor(y_train, dtype=torch.int64)

    def __len__(self):
        return len(self.y_train)
    
    def __getitem__(self, idx):
        return self.X_train[idx], self.y_train[idx]

#%%
train = FeatureDataset(f'./client-shards/client_{int(sys.argv[1])+1}.csv')
test = FeatureDataset('./data/mnist/mnist_test.csv')


#%%
trainset = torch.utils.data.DataLoader(train, batch_size=10, shuffle=True)
#%%
testset = torch.utils.data.DataLoader(test, batch_size=10, shuffle=True)

#%%
labels = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0, 9:0}
for index, data in enumerate(trainset):
  x, y = data[0][0], data[1][0]
  labels[y.item()]+=1
print("trainset distribution", labels)

#%%
class Client:
    def __init__(self, X, hidden_size):
        self.trainset = X
        self.f = torch.nn.Sequential(
            torch.nn.Linear(28*28, hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(hidden_size, 10)
        )
        self.loss = torch.nn.CrossEntropyLoss()

    def train(self, epochs):
        opt = torch.optim.SGD(self.f.parameters(), 0.25)

        for e in range(epochs):
            for data in self.trainset:
                X, y = data
                opt.zero_grad()
                f = self.f(X)
                loss = self.loss(f, y)
                loss.backward()
                opt.step()
            print(loss.item())

    def predict(self, x):
        pred = self.f(x)
        probs = torch.nn.functional.softmax(pred, dim=1)
        return torch.argmax(pred, axis=1)



#%%
c = Client(trainset, hidden_size=64)
c.train(10)

#%%
import pickle

state_dict = c.f.state_dict()
for key in state_dict:
    state_dict[key] = pickle.dumps(state_dict[key])

#%%
def measure_accuracy():
    correct = 0
    total = 0
    with torch.no_grad():
        for data in testset:
            X, y = data
            preds = torch.argmax(c.f(X.view(-1, 28*28)), axis=1)
            correct += torch.sum(preds == y).item()
            total += len(preds)
        accuracy = correct / total
        print("Testset accuracy: ", round(accuracy, 3))
measure_accuracy()
#%%
from secureClient import SecAggClient

end = 0.0

def receive_aggregated_weights(weights):
    global c, s, measure_accuracy, end, start
    end = time.time()
    print(f"Exchanging data took {end-start} seconds")
    c.f.load_state_dict(weights)
    c.train(10)
    measure_accuracy()
    # s.set_weights(c.f.state_dict())
    # s.start()

start = time.time()
s = SecAggClient("localhost", 5000, receive_aggregated_weights, False)
s.set_weights(c.f.state_dict())
s.configure(2, 100255)
s.start()
