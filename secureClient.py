import codecs
import json
import pickle
from copy import deepcopy
from random import randrange

import numpy as np
import socketio


class SecAggregator:
    def __init__(self, common_base, common_mod):
        self.secret_key = randrange(common_mod)
        self.base = common_base
        self.mod = common_mod
        self.public_key = (self.base ** self.secret_key) % self.mod
        self.individual_seed = randrange(common_mod)
        self.dim = dict()
        self.keys = dict()
        self.shared_secrets = dict()
        self.FIELD_SIZE = 10**5

    def set_weights(self, inner_state):
        self.weights = inner_state
        for key in inner_state:
            dim = np.shape(inner_state[key].numpy())
            if(len(dim) == 1):
                dim = (dim[0], 1)
                self.weights[key] = inner_state[key].reshape(dim[0], dim[1])
            self.dim[key] = dim

    def configure(self, base, mod):
        self.base = base
        self.mod = mod
        self.public_key = (self.base ** self.secret_key) % self.mod

    def __generate_coefficients(self, threshold: int, secret: int) -> list:
        coefficients = [randrange(0, self.FIELD_SIZE) for _ in range(threshold - 1)]
        coefficients.append(secret)
        return coefficients
    
    def __generate_shares(self, secret: int, threshold: int) -> dict:

        def __calculate_y(x: float, coefficients: list) -> float:
            point : float = 0
            for coefficient_index, coefficient_value in enumerate(coefficients[::-1]):
                point += x ** coefficient_index * coefficient_value
            return point

        coefficients = self.__generate_coefficients(threshold, secret)
        shares = dict()
        for key in self.keys:
            x = randrange(1, self.FIELD_SIZE)
            shares[key] = (x, __calculate_y(x, coefficients))
        return shares

    def __generate_weights(self, seed, dim):
        np.random.seed(seed)
        if(len(dim) < 2):
            dim = (dim[0], 1)
        return np.float32(np.random.rand(dim[0], dim[1]))

    def __prepare_shares(self, threshold: int, seed: int) -> dict:
        raw_shares = self.__generate_shares(seed, threshold)
        encrypted_shares = dict()
        for key in raw_shares:
             secret = self.shared_secrets[key]
             enc_x = raw_shares[key][0] + secret
             enc_y = raw_shares[key][1] + secret
             encrypted_shares[key] = (enc_x, enc_y)
        return encrypted_shares

    def prepare_secret_shares(self, threshold: int) -> dict:
        return self.__prepare_shares(threshold, self.individual_seed)

    def prepare_mask_shares(self, threshold: int) -> dict:
        return self.__prepare_shares(threshold, self.secret_key)

    def __mask_weight(self, weights, seed, multiplier=1):
        return {key: weights[key] + self.__generate_weights(seed, self.dim[key]) * multiplier for key in weights }

    def prepare_weights(self, id):
        masked_state = deepcopy(self.weights)
        for sid in self.keys:
            shared_seed = (self.keys[sid] ** self.secret_key) % self.mod
            self.shared_secrets[sid] = shared_seed
            if sid > id:
                masked_state = self.__mask_weight(masked_state,shared_seed)
            elif sid < id:
                masked_state = self.__mask_weight(masked_state, shared_seed, -1)
        masked_state = self.__mask_weight(masked_state, self.individual_seed)
        return masked_state
              
    def prepare_secrets(self, absentkeys: list, secrets:dict, masks: dict) -> dict:
        decrypted_shares = dict()
        for key in self.keys:
            x = secrets[key][0] - self.shared_secrets[key]
            y = secrets[key][1] - self.shared_secrets[key]
            decrypted_shares[key] = (x, y)
        for key in absentkeys:
            x = masks[key][0] - self.shared_secrets[key]
            y = masks[key][1] - self.shared_secrets[key]
            decrypted_shares[key] = (x, y)
        return decrypted_shares

class SecAggClient:
    def __init__(self,serverhost,serverport, on_end, enabled = True):
        self.host = serverhost
        self.port = serverport
        self.sio = socketio.Client()
        self.aggregator = SecAggregator(3,100103)
        self.counter = 0
        self.id = ''
        self.on_end = on_end
        self.enabled = enabled

    def start(self):
        self.register_handles()
        self.sio.connect(f'http://{self.host}:{self.port}')
        self.sio.emit('wakeup')

    def configure(self,b,m):
        self.aggregator.configure(b,m)

    def set_weights(self, inner_state):
        self.aggregator.set_weights(inner_state)

    def encoding(self, x):
        return codecs.encode(pickle.dumps(x), 'base64').decode()

    def decoding(self, s):
        return pickle.loads(codecs.decode(s.encode(),'base64'))

    def register_handles(self):
        @self.sio.event
        def connect():
            print('Connected to http://localhost:5000 with sid: ')

        @self.sio.event
        def identify(sid):
            print("identify sid:", sid)
            self.id = sid
            pubkey = {
                'key': self.aggregator.public_key
            }
            self.sio.emit('public_key', pubkey)
            

        @self.sio.on('broadcast_public_keys')
        def on_receive_public_keys(payload):
            if(self.enabled is False):
                resp = {
                    'weights': self.encoding(self.aggregator.weights),
                    'mask_shares': dict(),
                    'secret_shares': dict()
                }
                return self.sio.emit('transmit_data', resp)
            keys = json.loads(payload)
            self.aggregator.keys = keys
            masked_weights = self.aggregator.prepare_weights(self.id)
            encoded_weights = self.encoding(masked_weights)
            threshold = 2
            mask_shares = self.aggregator.prepare_mask_shares(threshold)
            encoded_mask = self.encoding(mask_shares)
            secret_shares = self.aggregator.prepare_secret_shares(threshold)
            encoded_shares = self.encoding(secret_shares)
            resp = {
                'weights': encoded_weights,
                'mask_shares': encoded_mask,
                'secret_shares': encoded_shares
            }
            self.sio.emit('transmit_data', resp)


        @self.sio.on('request_secret')
        def on_secret_requested(payload):
            secret_shares = self.decoding(payload['secret_shares'])
            mask_shares = self.decoding(payload['mask_shares'])
            absentkeys = json.loads(payload['missing'])
            secrets = self.aggregator.prepare_secrets(absentkeys, secret_shares, mask_shares)
            self.sio.emit('secret', self.encoding(secrets))

        @self.sio.event
        def end(payload):
            self.sio.disconnect()
            self.on_end(self.decoding(payload))

if __name__=="__main__":
    s = SecAggClient("localhost",5000)
    s.set_weights(np.zeros((10,10)), (10,10))
    s.configure(2, 100255)
    s.start()
