
import codecs
import json
import pickle
from decimal import Decimal
from math import ceil

import eventlet
import numpy as np
import socketio
import torch
from torchvision import datasets, transforms


class SecAggServer:
    def __init__(self, host, port, n, k, enabled = True):
        self.__default(n, k)
        self.host = host
        self.port = port
        self.sio = socketio.Server()
        self.app = socketio.WSGIApp(self.sio)
        self.socket = eventlet.listen(('', self.port))
        self.__register_handles()
        self.enabled = enabled

    def __default(self, n, k):
        self.base = 2
        self.mod = 100255
        self.n = n
        self.k = k
        self.responses = 0
        self.secretresp = 0
        self.absentset = set()
        self.resplist = []
        self.ready_client_ids = set()
        self.client_keys = dict()
        self.encrypted_secret_shares = dict()
        self.encrypted_mask_shares = dict()
        self.decrypted_shares = dict()

    def encoding(self, x):
        return codecs.encode(pickle.dumps(x), 'base64').decode()

    def decoding(self, s):
        return pickle.loads(codecs.decode(s.encode(),'base64'))


    def __register_handles(self):            
        @self.sio.on("connect")
        def handle_connect(sid, env, auth):
            self.ready_client_ids.add(sid)

        @self.sio.event
        def wakeup(sid):
            self.sio.emit('identify', sid, room=sid)

        @self.sio.on('public_key')
        def on_receive_public_key(sid, key):
            self.client_keys[sid] = key['key']
            self.absentset.add(sid)
            if(len(self.client_keys) == self.n):
                ready_clients = list(self.ready_client_ids)
                key_json = json.dumps(self.client_keys)
                for rid in ready_clients:
                    self.sio.emit('broadcast_public_keys', key_json, room=rid)

        @self.sio.on('transmit_data')
        def handle_client_data(sid, data):
            if self.responses < self.k:
                self.responses += 1
                client_weight = self.decoding(data['weights'])
                if self.enabled is True:
                    client_mask_shares = self.decoding(data['mask_shares'])
                    client_secret_shares = self.decoding(data['secret_shares'])
                    for key in client_secret_shares:
                        if key in self.encrypted_secret_shares:
                            self.encrypted_secret_shares[key][sid] = client_secret_shares[key]
                        else:
                            self.encrypted_secret_shares[key] = {sid: client_secret_shares[key]}
                    for key in client_mask_shares:
                        if key in self.encrypted_mask_shares:
                            self.encrypted_mask_shares[key][sid] = client_mask_shares[key]
                        else:
                            self.encrypted_mask_shares[key] = {sid: client_mask_shares[key]}

                if self.responses == 1:
                    self.aggregate = client_weight
                else:
                    self.aggregate = {key: self.aggregate[key] + client_weight[key] for key in self.aggregate}
                self.absentset.remove(sid)
                self.resplist.append(sid)
            else:
                resp = {'msg': 'Sent late, Disconnecting'}
                self.sio.emit('end', resp, room=sid)
            if(self.k == self.responses):
                if(self.enabled is False):
                    for key in self.aggregate:
                        self.aggregate[key] /= self.k
                        dim = np.shape(self.aggregate[key])
                        if(dim[1] == 1):
                            self.aggregate[key] = self.aggregate[key].reshape(dim[0],)   
                    for rid in self.resplist:
                        self.sio.emit('end', self.encoding(self.aggregate), room=rid)
                    return

                absentkeyjson = json.dumps(list(self.absentset))
                for rid in self.resplist:
                    resp = { 
                        'msg': 'Send secret shares',
                        'missing': absentkeyjson,
                        'mask_shares': self.encoding(self.encrypted_mask_shares[rid]),
                        'secret_shares': self.encoding(self.encrypted_secret_shares[rid])
                        }
                    self.sio.emit('request_secret', resp, room=rid)

        def calculate_intercept(shares: list) -> int:
            sums = 0
            for j, share_j in enumerate(shares):
                xj, yj = share_j
                prod = Decimal(1)
                for i, share_i in enumerate(shares):
                    xi, _ = share_i
                    if i != j:
                        prod *= Decimal(Decimal(xi)/(xi-xj))
                prod *= yj
                sums += Decimal(prod)
            return int(round(Decimal(sums), 0))
        
        def reconstruct_shared_mask(client, secret, dim):
            mask = np.zeros(dim)
            for key in self.client_keys:
                shared_seed = (self.client_keys[key] ** secret) % self.mod
                if key > client:
                    np.random.seed(shared_seed)
                    mask += np.float32(np.random.rand(dim[0], dim[1]))
                elif key < client:
                    np.random.seed(shared_seed)
                    mask -= np.float32(np.random.rand(dim[0], dim[1]))
            return mask


        def reconstruct_secret(shares: dict, dim: tuple) ->list:
            mask = np.zeros(dim)
            for key in shares:
                secret = calculate_intercept(shares[key])
                if key in self.absentset:
                    mask += reconstruct_shared_mask(key, secret, dim)
                else:
                    np.random.seed(secret)
                    mask -= np.float32(np.random.rand(dim[0], dim[1]))
            return mask

        @self.sio.on('secret')
        def handle_secret(sid, secrets):
            if(self.secretresp < self.k):
                client_shares = self.decoding(secrets)
                for key in client_shares:
                    if key in self.decrypted_shares:
                        self.decrypted_shares[key].append(client_shares[key])
                    else:
                        self.decrypted_shares[key] = [client_shares[key]]
                self.secretresp += 1
            if(self.secretresp == self.k):
                for key in self.aggregate:
                    masks = reconstruct_secret(self.decrypted_shares, np.shape(self.aggregate[key]))
                    self.aggregate[key] += masks
                    self.aggregate[key] /= self.k
                    dim = np.shape(self.aggregate[key])
                    if(dim[1] == 1):
                        self.aggregate[key] = self.aggregate[key].reshape(dim[0],)         
                for c_id in self.resplist:
                    self.sio.emit('end', self.encoding(self.aggregate), room=c_id)
                self.responses = 0
                self.aggregate = dict()

        @self.sio.on('disconnect')
        def handle_disconnect(sid):
            if sid in self.ready_client_ids:
                self.ready_client_ids.remove(sid)
                if len(self.ready_client_ids) == 0:
                    self.__default(self.n, self.k)
                    print('reset to default')

    def start(self):
        eventlet.wsgi.server(self.socket, self.app)


if __name__ == '__main__':
    server = SecAggServer("localhost", 5000, 2, 2, False)
    print("listening on http://localhost:5000")
    server.start()
