import time

import pandas as pd
import torch
from sklearn.preprocessing import Normalizer


class FeatureDataset(torch.utils.data.Dataset):
    def __init__(self, file_name):
        file_in = pd.read_csv(file_name, header=None)
        x = file_in.iloc[0:, 1:].values
        y = file_in.iloc[0:, 0].values

        x_train = Normalizer().transform(x)
        y_train = y
        self.X_train = torch.tensor(x_train, dtype=torch.float32)
        self.y_train = torch.tensor(y_train, dtype=torch.int64)

    def __len__(self):
        return len(self.y_train)
    
    def __getitem__(self, idx):
        return self.X_train[idx], self.y_train[idx]


train = FeatureDataset(f'./data/mnist/mnist_2000.csv')
test = FeatureDataset('./data/mnist/mnist_test.csv')

trainset = torch.utils.data.DataLoader(train, batch_size=10, shuffle=True)
testset = torch.utils.data.DataLoader(test, batch_size=10, shuffle=True)

class Client:
    def __init__(self, X, hidden_size):
        self.trainset = X
        self.f = torch.nn.Sequential(
            torch.nn.Linear(28*28, hidden_size),
            torch.nn.ReLU(),
            torch.nn.Linear(hidden_size, 10)
        )
        self.loss = torch.nn.CrossEntropyLoss()

    def train(self, epochs):
        opt = torch.optim.SGD(self.f.parameters(), 0.25)
        for e in range(epochs):
            for data in self.trainset:
                X, y = data
                opt.zero_grad()
                f = self.f(X)
                loss = self.loss(f, y)
                loss.backward()
                opt.step()
            print(loss.item())

    def predict(self, x):
        pred = self.f(x)
        return torch.argmax(pred, axis=1)

c = Client(trainset, hidden_size=64)
def measure_accuracy():
    correct = 0
    total = 0
    with torch.no_grad():
        for data in testset:
            X, y = data
            preds = torch.argmax(c.f(X.view(-1, 28*28)), axis=1)
            correct += torch.sum(preds == y).item()
            total += len(preds)
        accuracy = correct / total
        print("Testset accuracy: ", round(accuracy, 3))

start = time.time()
c.train(10)
end = time.time()
print(f"Trained for 10 epochs in: {end-start} seconds")
measure_accuracy()
