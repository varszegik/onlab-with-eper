#!/home/varszegikristof/inflated_location/Python-3.9.2/Python/bin/python3
import socketio
import eventlet
import pickle
import torch
from torchvision import datasets, transforms

sio = socketio.Server()
app = socketio.WSGIApp(sio)
socket = eventlet.listen(('', 5000))

class Server:
    def __init__(self):
        self.client_num = 0
        self.f = torch.nn.Sequential(
            torch.nn.Linear(28*28, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 10))

    def add_client_data(self, data):
        self.client_num += 1
        if(self.client_num > 1):
            self.client = torch.nn.Sequential(
                torch.nn.Linear(28*28, 64),
                torch.nn.ReLU(),
                torch.nn.Linear(64, 10)
            )
            self.client.load_state_dict(data)
            self.aggregate()
        else:
            self.f.load_state_dict(data)

    def aggregate(self):
        with torch.no_grad():
            for p in zip(*[self.f.parameters(), self.client.parameters()]):
                server_param, client_param = p
                summed = server_param.data * (self.client_num - 1) + client_param.data
                avg = summed / self.client_num
                for pj in p:
                    pj.data = torch.clone(avg)
            self.client = None

    def measure_accuracy(self):
        correct = 0
        total = 0
        with torch.no_grad():
            for data in testset:
                X, y = data
                preds = torch.argmax(self.f(X.view(-1, 28*28)), axis=1)
                correct += torch.sum(preds == y).item()
                total += len(preds)
            accuracy = correct / total
            print("Testset accuracy: ", round(accuracy, 3))

test = datasets.MNIST("", train=False, download=True, transform=transforms.Compose([transforms.ToTensor()]))
testset = torch.utils.data.DataLoader(test, batch_size=10, shuffle=True)


    

@sio.event
def connect(sid, environ, auth):
    pass
    #print('connect ', sid)

@sio.event
def send_weights_to_server(sid, data):
    global server
    for key in data:
        data[key] = pickle.loads(data[key])
    server.add_client_data(data)
    if(server.client_num == 299):
        server.measure_accuracy()
    else:
        print(server.client_num)
@sio.event
def disconnect(sid):
    pass
    #print('disconnect ', sid)


if __name__ == '__main__':
    server = Server()
    eventlet.wsgi.server(socket, app)