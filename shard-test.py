import pickle
import numpy as np

def readClientData(client):
    with open("client-shards/client_{}.pkl".format(client), "rb") as fh:
        return pickle.load(fh)

def readTestData():
    with open("data/mnist/test_formatted.pkl", "rb") as fh:
        return pickle.load(fh)

data = readClientData(1)
training_image_data = data[0]
training_label_data = data[1]
training_labels_one_hot = data[2]

data = readTestData()
test_image_data = data[0]
test_label_data = data[1]
test_labels_one_hot = data[2]
print("format, load and train")


