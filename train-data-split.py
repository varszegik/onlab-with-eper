import random

import numpy as np


def createClientShards(data, num_clients=3):
    initial = "client"
    client_names = ['{}_{}'.format(initial, i + 1) for i in range(num_clients)]
    random.shuffle(data)

    #add shard to each client
    shard_size = len(data)//num_clients
    shards = [data[i:i + shard_size] for i in range(0, shard_size * num_clients, shard_size)]
    
    #number of clients = number of shards
    assert(len(shards) == len(client_names))

    return {client_names[i] : shards[i] for i in range(num_clients)}

no_labels = 10

data_path = "data/mnist/"
train_data = np.loadtxt(data_path +"mnist_2000.csv", delimiter=",")

shards_dict = createClientShards(train_data, 10)

for key in shards_dict:
    np.savetxt('client-shards/{}.csv'.format(key), shards_dict[key], delimiter=",")
